﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unibo.Oop.Events
{
    class OrderedEventSource<TArg> : AbstractEventSourceImpl<TArg>
    {
        private IList<EventListener<TArg>> eventListeners = new List<EventListener<TArg>>();
        protected override ICollection<EventListener<TArg>> getEventListeners()
        {
            return eventListeners;
        }
    }


}
