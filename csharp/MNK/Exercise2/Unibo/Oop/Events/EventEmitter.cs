﻿using System;
namespace Unibo.Oop.Events
{
    public static class EventEmitter
    {
        public static IEventEmitter<TArg> Ordered<TArg>() 
        {
            return (IEventEmitter<TArg>) new OrderedEventSource<TArg>();
        }
    }
}
