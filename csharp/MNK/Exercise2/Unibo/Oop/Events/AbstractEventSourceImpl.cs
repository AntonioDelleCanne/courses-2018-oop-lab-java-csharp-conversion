﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unibo.Oop.Events
{
    abstract class AbstractEventSourceImpl<TArgs> : IEventSource<TArgs>, IEventEmitter<TArgs>
    {
        public IEventSource<TArgs> EventSource
        {
            get { return this;}
        }

        protected abstract ICollection<EventListener<TArgs>> getEventListeners();
        public void Bind(EventListener<TArgs> eventListener)
        {
            getEventListeners().Add(eventListener);
        }

        public void Unbind(EventListener<TArgs> eventListener)
        {
            getEventListeners().Remove(eventListener);
        }

        public void UnbindAll()
        {
            getEventListeners().Clear();
        }

        public void Emit(TArgs data)
        {
            foreach (EventListener<TArgs> eventListener in getEventListeners())
            {
                eventListener(data);
            }
        }
    }
}
