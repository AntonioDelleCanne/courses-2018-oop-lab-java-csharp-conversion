﻿using System;
using System.Linq;

namespace Unibo.Oop.Utils
{
    internal class TupleImpl : ITuple
    {
        private object[] array;

        public TupleImpl(object[] args)
        {
            this.array = args;
        }

        public object this[int i] {
            get { return this.array[i]; }
        }

        public int Length
        {
            get { return this.array.Length; }
        }

        public object[] ToArray()
        {
            object[] result = new object[this.array.Length];
            this.array.CopyTo(result, 0);
            return result;
        }

        public override String ToString()
        {
            return "(" + Array.ConvertAll<object, string>(this.array, new Converter<object, string>(e => e.ToString())).Aggregate((s1,s2) => s1 + ", " + s2) + ")";
        }

        public override bool Equals(object obj)
        {
            return obj != null
                && obj is TupleImpl
                && Enumerable.SequenceEqual(this.array, ((TupleImpl)obj).array);
        }

        public override int GetHashCode()
        {
            const int prime = 31;
            int result = 1;
            foreach (Object obj in this.array)
            {
                result = prime * result + obj.GetHashCode();
            }
            return result;
        }
    }
}
